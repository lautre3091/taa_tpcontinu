Projet TAA 2015-2016
===================
Ce TP nous a fais utiliser plusieur technologie pour nous montrer comment mettre en place une stack backend  

----------
[TOC]

----------

##1. Présentation

En utilisant *JPA*, *JAX-RS*, *Spring-boot* entre autre nous avons mit en place une stack back-end.

----------

##2. Ce qui est fais

 - La mise en place des annotation **JPA** sur les classes métier.
 - Le developpement des services et repository pour l'utilisation des entitées.
 - La mise en place d'un service **REST** avec *JAX-RS*.
 - L'utilisation de *Spring-boot*
 - L'ecriture d'un Dockerfile pour la création d'un container.
 - Utilisation d'une base de donnée MySQL (ISTIC)
 
----------

##3. Ce qui reste à faire

 - Comprendre comment créer le schema de base dans le container docker MySQL (probleme avec le create et update)
 - Finaliser l'ecriture du fichier docker-compose.yml.

----------

##4. Commande docker pour linker 
 
 - Lancement du container mysql avec création de la base *taaDataBase*
    `docker run --name mysqlTAA -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=taaDataBase mysql:latest`
 - Link de notre application avec le container mysql (name= mysqlTAA)
    `docker run -it -p 8080:8080 --name taa-app --link mysqlTAA:mysql lautre/taa-tp:latest`
    
##5. Probléme 

  J'ai rencontrer quelque que soucis que je n'arrive pas a reglé. Pour commencer, initialiser la BDD. J'ai trouvé 2 solution pour cela mais elle ne sont pas facile a automatiser et a metre en oeuvre. et une troisiéme que je n'ait pas eu le temps de mettre en oeuvre
  Il faut commancer par créer une image avec la valeur 'create' pour le champ 'hibernate.hbm2ddl.auto' du fichier persistence.xml et de lancer un container avec cette image pour initialiser la BDD, C'est ici que les deux solutions diverge.
    - La premiére est de stoper le container et créer une nouvelle image avec la valeur 'update' pour le champ 'hibernate.hbm2ddl.auto' et lancer un container avec cette nouvelle image.
    - La seconde est de copier un fichier persistance.xml avec la valeur 'update' pour le champ 'hibernate.hbm2ddl.auto' dans le container qui est lancé, puis de relancer ce container. 
  La toisiéme et derniére solution serais de créer un script d'initialisation de base de donnée à lancer avant de lancer le container d'application avec la valeur 'update' pour le champ 'hibernate.hbm2ddl.auto' du fichier persistence.xml.                        
  Le seconde probléme que j'ai rencontré concerne le fichier docker-compose.yml, il semble qu'il est bon mais lorsque que je le lance il semble ne pas fonctionner.