package fr.lautre.istic.taa.tp.web.rest;

import fr.lautre.istic.taa.tp.domain.Sprint;
import fr.lautre.istic.taa.tp.service.SprintService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lautre on 14/10/15.
 */
@ComponentScan("fr.lautre.istic.taa.tp.service")
@RestController
@RequestMapping("/sprint")
@Api(value="/sprint", description = "Sprint resource")
public class SprintRessource extends GenericRessource<Sprint>{

    @Autowired
    private SprintService sprintService;

    @RequestMapping(method = RequestMethod.POST, value = "/create/{name}")
    public void postCreateSprint(@PathVariable("name") String name) {
        sprintService.create(name);
    }
}


