package fr.lautre.istic.taa.tp.web.rest;

import fr.lautre.istic.taa.tp.domain.UserStorie;
import fr.lautre.istic.taa.tp.service.UserStorieService;
import io.swagger.annotations.Api;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lautre on 25/09/15.
 */
@ComponentScan("fr.lautre.istic.taa.tp.service")
@RestController
@RequestMapping("/userStorie")
@Api(value="/userStorie", description = "UserStorie resource")
public class UserStorieRessource extends GenericRessource<UserStorie>{

    @Autowired
    private UserStorieService userStorieService;

    @RequestMapping(method = RequestMethod.POST, value = "/create/{sprintId}/{userId}", consumes="application/json")
    public void postCreateUserStorie(@RequestBody String string,
                                  @PathVariable("userId") Long userId,
                                  @PathVariable("sprintId") Long sprintID) throws JSONException {
        JSONObject jsonObject = new JSONObject(string);
        userStorieService.create(jsonObject.getString("description"),jsonObject.getString("name"), userId, sprintID);
    }
}
