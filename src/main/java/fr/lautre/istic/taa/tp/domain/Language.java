package fr.lautre.istic.taa.tp.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import fr.lautre.istic.taa.tp.domain.util.CustomListSerializer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lautre on 29/10/15.
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Language {

    private List<User> users = new ArrayList<>();
    private String name;
    private Long id;

    public Language() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "languages")
    @JsonSerialize(using = CustomListSerializer.class)
    public List<User> getUsers() {
        return users;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void removeUser(User user) {
        users.remove(user);
    }
}
