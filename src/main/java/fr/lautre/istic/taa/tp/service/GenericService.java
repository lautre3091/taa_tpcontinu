package fr.lautre.istic.taa.tp.service;

import fr.lautre.istic.taa.tp.repository.GenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Configuration
@ComponentScan("fr.lautre.istic.taa.tp.repository")
@Service
public abstract class GenericService<T> {

    @Autowired
    GenericRepository<T> repository;

    public List<T> findAll(){
        return repository.findAll();
    }
    public Optional<T> findOneById(Long id){
        return repository.findOneById(id);
    }
    public void merge(T t){
        repository.merge(t);
    }

    abstract void removeOne(T t);

    public void removeOneById(Long id) {
        Optional<T> optional = findOneById(id);
        if (optional.isPresent()) {
            removeOne(optional.get());
        }

    }
}
