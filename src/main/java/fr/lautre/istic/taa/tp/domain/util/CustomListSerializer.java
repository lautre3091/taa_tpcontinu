package fr.lautre.istic.taa.tp.domain.util;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.lautre.istic.taa.tp.domain.User;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lautre on 10/01/16.
 */
public class CustomListSerializer extends JsonSerializer<List<User>> {

    @Override
    public void serialize(List<User> users, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        List<Long> ids = users.stream().map(User::getId).collect(Collectors.toList());
        jsonGenerator.writeObject(ids);
    }
}
