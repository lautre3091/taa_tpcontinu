package fr.lautre.istic.taa.tp.repository;

import fr.lautre.istic.taa.tp.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository extends GenericRepository<User>{

    public UserRepository(){
        super();
        setEntityManager(factory.createEntityManager());
    }

    @Override
    public List<User> findAllAux(){
        return entityManager.createQuery("select u from User u", User.class).getResultList();
    }

    @Override
    public Optional<User> findOneByIdAux(Long id){
        return Optional.of(entityManager.createQuery("select u from User u where id=:id", User.class)
                    .setParameter("id", id)
                    .getSingleResult());
    }
}
