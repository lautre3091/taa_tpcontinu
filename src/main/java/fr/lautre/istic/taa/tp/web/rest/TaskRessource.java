package fr.lautre.istic.taa.tp.web.rest;

import fr.lautre.istic.taa.tp.domain.Task;
import fr.lautre.istic.taa.tp.service.TaskService;
import io.swagger.annotations.Api;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lautre on 14/10/15.
 */
@ComponentScan("fr.lautre.istic.taa.tp.service")
@RestController
@RequestMapping("/task")
@Api(value="/task", description = "Task resource")
public class TaskRessource extends GenericRessource<Task>{

    @Autowired
    private TaskService taskService;

    @RequestMapping(method = RequestMethod.POST, value = "/create/{userStorieId}")
    public void postCreateTask(@PathVariable("userStorieId") Long userStorieId,
                               @RequestBody String string) throws JSONException {
        JSONObject jsonObject = new JSONObject(string);
        taskService.create(jsonObject.getString("name"),jsonObject.getString("description"), userStorieId);
    }
}



