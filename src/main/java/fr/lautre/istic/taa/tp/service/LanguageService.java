package fr.lautre.istic.taa.tp.service;

import fr.lautre.istic.taa.tp.domain.Language;
import fr.lautre.istic.taa.tp.domain.User;
import fr.lautre.istic.taa.tp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by lautre on 29/10/15.
 */

@Service
@ComponentScan("fr.lautre.istic.taa.tp.repository")
public class LanguageService extends GenericService<Language> {

    @Autowired
    private UserRepository userRepository; 

    @Override
    public void removeOne(Language language) {
        for(User user : language.getUsers()){
            user.getLanguages().remove(language);
            userRepository.merge(user);
        }

        repository.remove(language);
    }

    public void create(String name) {
        repository.add(new Language(name));
    }

    public void createWithOneUser(String name, Long idUser) {
        Optional<User> oUser = userRepository.findOneByIdAux(idUser);
        Language language = new Language(name);
        if (oUser.isPresent()) {
            User user = oUser.get();
            language.addUser(user);
            repository.add(language);
            user.addLanguage(language);
            userRepository.merge(user);
        }
    }

    public void addUser(Long idLanguage, Long idUser) {
        Optional<Language> oLanguage = repository.findOneByIdAux(idLanguage);
        if (oLanguage.isPresent()){
            repository.merge(findAndAddUser(oLanguage.get(),idUser));
        }
    }

    public void removeUser(Long idLanguage, Long idUser) {
        Optional<Language> oLanguage = repository.findOneByIdAux(idLanguage);
        if (oLanguage.isPresent()){
            repository.merge(findAndRemoveUser(oLanguage.get(), idUser));
        }
    }

    private Language findAndAddUser(Language language, Long idUser){
        Optional<User> oUser = userRepository.findOneByIdAux(idUser);
        if (oUser.isPresent()) {
            User user = oUser.get();
            user.addLanguage(language);
            userRepository.merge(user);
            language.addUser(user);
        }
        return language;
    }

    private Language findAndRemoveUser(Language language, Long idUser){
        Optional<User> oUser = userRepository.findOneByIdAux(idUser);
        if (oUser.isPresent()) {
            User user = oUser.get();
            user.removeLanguage(language);
            userRepository.merge(user);
            language.removeUser(user);
        }
        return language;
    }
}
