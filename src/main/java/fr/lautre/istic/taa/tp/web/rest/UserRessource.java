package fr.lautre.istic.taa.tp.web.rest;

import fr.lautre.istic.taa.tp.domain.User;
import fr.lautre.istic.taa.tp.service.UserService;
import io.swagger.annotations.Api;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lautre on 25/09/15.
 */
@ComponentScan("fr.lautre.istic.taa.tp.service")
@RestController
@RequestMapping("/user")
@Api(value="/user", description = "User resource")
public class UserRessource extends GenericRessource<User>{

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST, value = "/create", consumes="application/json")
    public void postCreateUser(@RequestBody String string) throws JSONException {
        JSONObject jsonObject = new JSONObject(string);
        userService.create(jsonObject.getString("firstname"), jsonObject.getString("lastname"));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/add/{idUser}/{idLanguage}")
    public void postAddUser(@PathVariable("idUser") Long idUser, @PathVariable("idLanguage") Long idLanguage) {
        userService.addLanguage(idUser, idLanguage);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/remove/{idUser}/{idLanguage}")
    public void postRemoveUser(@PathVariable("idUser") Long idUser, @PathVariable("idLanguage") Long idLanguage) {
        userService.removeLanguage(idUser, idLanguage);
    }
}
