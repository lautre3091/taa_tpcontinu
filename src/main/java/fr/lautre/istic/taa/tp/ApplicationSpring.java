package fr.lautre.istic.taa.tp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import java.net.UnknownHostException;

/**
 * Created by lautre on 25/09/15.
 */
@ComponentScan
@EnableAutoConfiguration
public class ApplicationSpring {

    public static void main( String[] args ) throws UnknownHostException {
        SpringApplication app = new SpringApplication(ApplicationSpring.class);
        app.run(args);
    }
}
