package fr.lautre.istic.taa.tp.repository;

import fr.lautre.istic.taa.tp.domain.UserStorie;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by lautre on 25/09/15.
 */
@Repository
public class UserStorieRepository extends GenericRepository<UserStorie>{

    public UserStorieRepository(){
        super();
        setEntityManager(factory.createEntityManager());
    }

    @Override
    public List<UserStorie> findAllAux(){
        return entityManager.createQuery("SELECT us FROM UserStorie us",UserStorie.class).getResultList();
    }

    @Override
    public Optional<UserStorie> findOneByIdAux(Long id){
        return Optional.of(entityManager.createQuery("select us from UserStorie us where id=:id", UserStorie.class)
                        .setParameter("id", id)
                        .getSingleResult());
    }
}
