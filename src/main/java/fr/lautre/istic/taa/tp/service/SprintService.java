package fr.lautre.istic.taa.tp.service;

import fr.lautre.istic.taa.tp.domain.Sprint;
import fr.lautre.istic.taa.tp.domain.UserStorie;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by lautre on 22/10/15.
 */
@Service
@ComponentScan("fr.lautre.istic.taa.tp.repository")
public class SprintService extends GenericService<Sprint> {

    public void create(String name) {
        repository.add(new Sprint(name, new Date(), new Date()));
    }

    @Override
    public void removeOne(Sprint sprint) {
        List<UserStorie> userStories = sprint.getUserStories();
        UserStorieService userStorieService = new UserStorieService();
        userStories.forEach(userStorieService::removeUser);
        userStories.clear();

        repository.remove(sprint);
    }

}
