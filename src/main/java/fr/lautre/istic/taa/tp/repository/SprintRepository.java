package fr.lautre.istic.taa.tp.repository;

import fr.lautre.istic.taa.tp.domain.Sprint;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by lautre on 14/10/15.
 */
@Repository
public class SprintRepository extends GenericRepository<Sprint> {

    public SprintRepository(){
        super();
        setEntityManager(factory.createEntityManager());
    }

    @Override
    public List<Sprint> findAllAux() {
        return entityManager.createQuery("select s from Sprint s", Sprint.class).getResultList();
    }

    @Override
    public Optional<Sprint> findOneByIdAux(Long id) {
        return Optional.of(entityManager.createQuery("select s from Sprint s where id=:id", Sprint.class)
                    .setParameter("id", id)
                    .getSingleResult());
    }

}
