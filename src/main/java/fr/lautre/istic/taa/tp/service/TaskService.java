package fr.lautre.istic.taa.tp.service;

import fr.lautre.istic.taa.tp.domain.Task;
import fr.lautre.istic.taa.tp.domain.UserStorie;
import fr.lautre.istic.taa.tp.repository.UserStorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by lautre on 22/10/15.
 */

@Service
@ComponentScan("fr.lautre.istic.taa.tp.repository")
public class TaskService extends GenericService<Task> {

    @Autowired
    UserStorieRepository userStorieRepository;

    @Override
    public void removeOne(Task task) {
        UserStorie userStorie = task.getUserStorie();
        userStorie.getTasks().remove(task);
        userStorieRepository.merge(userStorie);
    }

    public void create(String name, String description, Long id) {
        Task task = new Task(name,description);
        Optional<UserStorie> oUserStorie = userStorieRepository.findOneByIdAux(id);
        if (oUserStorie.isPresent()) {
            task.setUserStorie(oUserStorie.get());
            repository.add(task);
        }
    }
}
