package fr.lautre.istic.taa.tp.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created by lautre on 09/12/15.
 */

@Component
@Aspect
public class LoggingAspect {

    @Before("execution(* fr..*Ressource.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        System.out.println("IN \t\t" + getClass(joinPoint.getThis().toString()) + "\t\t-->\t" + joinPoint.getSignature().getName()+"()");
    }

    @After("execution(* fr..*Ressource.*(..))")
    public void logAfter(JoinPoint joinPoint) {
        System.out.println("OUT \t" + getClass(joinPoint.getThis().toString()) + "\t\t-->\t" + joinPoint.getSignature().getName()+"()");
    }

    private String getClass(String s){
        return s.substring(s.lastIndexOf(".")+1,s.indexOf("@"));
    }
}
