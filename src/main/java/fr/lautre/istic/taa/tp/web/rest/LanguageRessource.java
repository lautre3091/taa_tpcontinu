package fr.lautre.istic.taa.tp.web.rest;

import com.wordnik.swagger.annotations.ApiOperation;
import fr.lautre.istic.taa.tp.domain.Language;
import fr.lautre.istic.taa.tp.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lautre on 29/10/15.
 */
@ComponentScan("fr.lautre.istic.taa.tp.service")
@RestController
@RequestMapping("/language")
public class LanguageRessource extends GenericRessource<Language> {

    @Autowired
    private LanguageService languageService;

    @RequestMapping(method = RequestMethod.POST, value = "/create/{name}/{idUser}")
    @ApiOperation(httpMethod = "POST",value="Create a language with on user")
    public void postCreateLanguageWithOneUser(@PathVariable("name") String name, @PathVariable("idUser") Long idUser) {
        languageService.createWithOneUser(name, idUser);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create/{name}")
    public void postCreateLanguage(@PathVariable("name") String name) {
        languageService.create(name);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/add/{idLanguage}/{idUser}")
    public void postAddUser(@PathVariable("idUser") Long idUser, @PathVariable("idLanguage") Long idLanguage) {
        languageService.addUser(idLanguage, idUser);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/remove/{idLanguage}/{idUser}")
    public void postRemoveUser(@PathVariable("idUser") Long idUser, @PathVariable("idLanguage") Long idLanguage) {
        languageService.removeUser(idLanguage, idUser);
    }
}
