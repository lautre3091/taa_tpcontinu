package fr.lautre.istic.taa.tp.service;

import fr.lautre.istic.taa.tp.domain.Sprint;
import fr.lautre.istic.taa.tp.domain.Task;
import fr.lautre.istic.taa.tp.domain.User;
import fr.lautre.istic.taa.tp.domain.UserStorie;
import fr.lautre.istic.taa.tp.repository.SprintRepository;
import fr.lautre.istic.taa.tp.repository.UserRepository;
import fr.lautre.istic.taa.tp.repository.UserStorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by lautre on 22/10/15.
 */

@Service
@ComponentScan("fr.lautre.istic.taa.tp.repository")
public class UserStorieService extends GenericService<UserStorie> {

    UserStorieService (){
        repository = new UserStorieRepository();
    }

    @Autowired
    UserRepository userRepository = new UserRepository();

    @Autowired
    SprintRepository sprintRepository= new SprintRepository();


    public void create(String description, String name, Long userId, Long sprintId) {

        UserRepository userRepository = new UserRepository();
        Optional<User> user = userRepository.findOneByIdAux(userId);

        SprintRepository sprintRepository = new SprintRepository();
        Optional<Sprint> sprint = sprintRepository.findOneByIdAux(sprintId);

        if (user.isPresent()&&sprint.isPresent()) {
            UserStorie userStorie= new UserStorie(name,user.get(),description);
            userStorie.setSprint(sprint.get());

            repository.add(userStorie);
        }
    }

    @Override
    public void removeOne(UserStorie userStorie) {
        /**Tasks**/
        List<Task> tasks = userStorie.getTasks();
        tasks.clear();

        /**User**/
        removeUser(userStorie);

       /**Sprint**/
        removeSprint(userStorie);

        repository.remove(userStorie);
    }

    public void removeUser(UserStorie userStorie){
        User user = userStorie.getUser();
        user.getUserStories().remove(userStorie);
    }

    public void removeSprint(UserStorie userStorie){
        Sprint sprint = userStorie.getSprint();
        sprint.getUserStories().remove(userStorie);
    }
}
