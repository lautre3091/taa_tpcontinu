package fr.lautre.istic.taa.tp.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lautre on 21/09/15.
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserStorie {

    private String description;
    private Long id;
    private User user;
    private List<Task> tasks;
    private String name;

    public UserStorie(String name,User user, String description) {
        this.name = name;
        this.user = user;
        this.description = description;
        this.tasks= new ArrayList<>();
    }

    public UserStorie() {
    }

    @ManyToOne(optional = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToMany(fetch = FetchType.EAGER,
            orphanRemoval=true,
            mappedBy = "userStorie",
            cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    private Sprint sprint;

    @ManyToOne(optional = false)
    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addTasks(Task task){
        this.tasks.add(task);
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}
