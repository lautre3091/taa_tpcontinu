package fr.lautre.istic.taa.tp.web.rest;

import fr.lautre.istic.taa.tp.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by lautre on 23/10/15.
 */
@Configuration
@ComponentScan("fr.lautre.istic.taa.tp.service")
@RestController
public abstract class GenericRessource<T> {

    @Autowired
    protected GenericService<T> service;

    @RequestMapping(value = "/all",
            method = RequestMethod.GET,
            produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    public List getAll() {
        return service.findAll();
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    public Response getById(@PathVariable("id") Long id) {
        return service.findOneById(id)
                .map(e -> Response.ok().entity(e).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        service.removeOneById(id);
    }
}
