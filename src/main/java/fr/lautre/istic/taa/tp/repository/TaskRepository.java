package fr.lautre.istic.taa.tp.repository;

import fr.lautre.istic.taa.tp.domain.Task;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by lautre on 14/10/15.
 */
@Repository
public class TaskRepository extends GenericRepository<Task> {

    public TaskRepository(){
        super();
        setEntityManager(factory.createEntityManager());
    }

    @Override
    public List<Task> findAllAux() {
        return entityManager.createQuery("select t from Task t", Task.class).getResultList();
    }

    @Override
    public Optional<Task> findOneByIdAux(Long id) {
        return Optional.of(entityManager.createQuery("select u from Task u where id=:id", Task.class)
                    .setParameter("id", id)
                    .getSingleResult());
    }
}
