package fr.lautre.istic.taa.tp.repository;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public abstract class GenericRepository<T> {

    static final protected EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
    protected EntityManager entityManager;

    public GenericRepository() {}

    public abstract List<T> findAllAux();
    public abstract Optional<T> findOneByIdAux(Long id);

    public void add(T t){
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        try {
            entityManager.persist(t);
            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(T t){
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        try {
            entityManager.remove(entityManager.merge(t));
            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void merge(T t){
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        try {
            entityManager.merge(t);
            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets new entityManager.
     *
     * @param entityManager New value of entityManager.
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<T> findAll(){
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        List<T> tList = new ArrayList<>();
        try {
            tList = findAllAux();
            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tList;
    }

    public Optional<T> findOneById(Long id){
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Optional<T> tOptional = Optional.empty();
        try {
            tOptional = findOneByIdAux(id);
            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tOptional;
    }



}
