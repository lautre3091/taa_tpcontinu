package fr.lautre.istic.taa.tp.repository;

import fr.lautre.istic.taa.tp.domain.Language;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by lautre on 29/10/15.
 */

@Repository
public class LanguageRepository extends GenericRepository<Language> {

    public LanguageRepository(){
        super();
        setEntityManager(factory.createEntityManager());
    }

    @Override
    public List<Language> findAllAux() {
       return entityManager.createQuery("select s from Language s", Language.class).getResultList();
    }

    @Override
    public Optional<Language> findOneByIdAux(Long id) {
       return Optional.of(entityManager.createQuery("select l from Language l where id=:id", Language.class)
                    .setParameter("id", id)
                    .getSingleResult());
    }
}
