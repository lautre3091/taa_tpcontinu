package fr.lautre.istic.taa.tp.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lautre on 21/09/15.
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class User {

    public User(String prenom, String nom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    private String nom;
    private String prenom;
    private List<UserStorie> userStories = new ArrayList<>();
    private List<Language> languages = new ArrayList<>();

    public User() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @OneToMany(
            fetch = FetchType.EAGER,
            orphanRemoval=true,
            cascade = {CascadeType.PERSIST,CascadeType.REMOVE},
            mappedBy = "user")
    public List<UserStorie> getUserStories() {
        return userStories;
    }

    public void setUserStories(List<UserStorie> userStories) {
        this.userStories = userStories;
    }

    private Long id;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public void addLanguage(Language language) {
        languages.add(language);
    }

    public void removeLanguage(Language language) {
        languages.remove(language);
    }
}
