package fr.lautre.istic.taa.tp.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by lautre on 21/09/15.
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Sprint {

    private String nom;
    private Date start;
    private Date end;
    private List<UserStorie> userStories;

    public Sprint(String nom, Date start, Date end) {
        this.nom = nom;
        this.start = start;
        this.end = end;
    }

    public Sprint() {
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    @OneToMany(fetch = FetchType.EAGER,
            orphanRemoval=true,
            mappedBy = "sprint",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    public List<UserStorie> getUserStories() {
        return userStories;
    }

    public void setUserStories(List<UserStorie> userStories) {
        this.userStories = userStories;
    }

    private Long id;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "{Id : "+id+", Nom : "+ nom +" }";
    }
}
