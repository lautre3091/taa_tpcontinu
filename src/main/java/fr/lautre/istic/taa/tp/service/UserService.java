package fr.lautre.istic.taa.tp.service;

import fr.lautre.istic.taa.tp.domain.Language;
import fr.lautre.istic.taa.tp.domain.User;
import fr.lautre.istic.taa.tp.domain.UserStorie;
import fr.lautre.istic.taa.tp.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@ComponentScan("fr.lautre.istic.taa.tp.repository")
public class UserService extends GenericService<User> {

    @Autowired
    private LanguageRepository languageRepository;

    @Override
    public void removeOne(User user) {
        /**UserStories**/
        List<UserStorie> userStories = user.getUserStories();
        UserStorieService userStorieService = new UserStorieService();
        userStories.forEach(userStorieService::removeSprint);
        userStories.clear();

        for (Language language : user.getLanguages()){
            language.removeUser(user);
            languageRepository.merge(language);
        }
        user.getLanguages().clear();

        repository.remove(user);
    }


    public void create(String firstname, String lastname) {
        repository.add(new User(firstname, lastname));

    }

    public void addLanguage(Long idUser, Long idLanguage) {
        Optional<User> oUser = repository.findOneByIdAux(idUser);
        if (oUser.isPresent()){
            repository.merge(findAndAddLanguage(oUser.get(), idLanguage));
        }
    }

    public void removeLanguage(Long idUser, Long idLanguage) {
        Optional<User> oUser = repository.findOneByIdAux(idUser);
        if (oUser.isPresent()){
            repository.merge(findAndRemoveLanguage(oUser.get(), idLanguage));
        }
    }

    private User findAndAddLanguage(User user, Long idLanguage){
        Optional<Language> oLanguage = languageRepository.findOneByIdAux(idLanguage);
        if (oLanguage.isPresent()) {
            Language language = oLanguage.get();
            user.addLanguage(language);
            language.addUser(user);
            languageRepository.merge(language);
        }
        return user;
    }

    private User findAndRemoveLanguage(User user, Long idLanguage){
        Optional<Language> oLanguage = languageRepository.findOneByIdAux(idLanguage);
        if (oLanguage.isPresent()) {
            Language language = oLanguage.get();
            user.removeLanguage(language);
            language.removeUser(user);
            languageRepository.merge(language);
        }
        return user;
    }
}
